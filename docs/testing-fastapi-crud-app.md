### pytest와 httpx 설치

```bash
$ pip install pytest
$ pip install httpx
```

`tests/__init__.py`와 `tests/test_views.py` 파일을 생성하고 `test_views.py`를 열어 필요한 import 이후 다음 코드를 추가한다.

**postgreSQL에 db_test_news 데이터베이스를 생성하는 것을 잊지 마세요.**

```python
# tests/test_views.py

import os
import json
from fastapi import status
from fastapi.testclient import TestClient
from datetime import datetime as dt
import pytest
#  internals
from app.main import app
from app.database import create_tables, insert_t_news, drop_tables
from app import models

client = TestClient(app)


#  use test database and create tables
os.environ["DB_NAME"] = "db_test_news"
drop_tables()
create_tables()
```

- 환경 변수 조작을 위한 `os`, JSON 데이터 작업을 위한 `json`, HTTP 상태 코드를 위한 `fastapi`의 `status`, FastAPI 어플리케이션을 테스트하기 위한 `fastapi.testclient`의 `TestClient`, 날짜 및 시간 값 작업을 위한 `datetime`, 테스트 실행을 위한 `pytest` 등 필요한 임포트를 한다.
- `app.main` 모듈에서 `app` 객체를 임포트한다. 이는 테스트에 사용될 FastAPI 어플리케이션 객체이다.
- `app.database` 모듈에서 `create_tables`, `insert_t_news` 및 `drop_tables` 함수를 임포트한다. 이 함수들은 데이터베이스에서 테이블을 만들고 삭제한다.
- `app` 패키지에서 `models` 모듈을 임포트한다. 이 모듈에는 어플리케이션에서 사용되는 데이터 모델이 포함되어 있다.
- `app` 객체로 `TestClient`의 인스턴스를 생성하고 `client` 변수에 할당한다. 이 클라이언트는 어플리케이션에 테스트 요청을 하는 데 사용된다.
환경 변수 `DB_NAME`을 `"db_test_news"`로 설정한다. 이 변수는 사용할 테스트 데이터베이스의 이름을 지정할 가능성이 높다.
`drop_tables` 함수는 테스트 데이터베이스의 테이블을 삭제하기 위해 호출된다.
테스트 데이터베이스의 테이블을 생성하기 위해 `create_tables` 함수를 호출한다.

위의 코드는 테스트 데이터베이스 테이블 생성을 포함하여 FastAPI 어플리케이션을 테스트하는 데 필요한 테스트 환경을 설정한다. `TestClient` 인스턴스는 어플리케이션에 테스트 요청을 할 준비가 되었다.

```python
# test/test_views.py

# imports ...
def test_read_news():
    response = client.get("/news")
    assert response.status_code == status.HTTP_200_OK
```

- `GET /news` 엔드포인트에 대한 테스트 케이스로 `test_read_news` 함수를 정의한다.
- `/news` 엔드포인트에 GET 요청을 하는 데 `client.get` 메서드를 사용한다.
- `response` 객체는 서버에서 받은 응답을 나타낸다.
- `assert` 문은 응답 상태 코드가 `200 OK`(`status.HTTP_200_OK`)인지 확인하는 데 사용된다. 어설션을 통과하면 테스트 케이스가 성공한 것이며, 어설션이 실패하면 어설션 오류가 발생한다.

이 테스트 케이스는 `/news` 엔드포인트가 상태 코드가 `200 OK`인 성공적인 응답을 반환하는지 확인하는 것이다. 필요한 경우 어설션을 더 추가하여 응답 본체 또는 응답의 다른 측면의 유효성을 검사할 수 있다.

```python
# tests/test_views.py

def test_create_news():
    payload = {
        'created_by': 'adnankaya', 'context': 'contex1', 'published_date': '05-23-2023 15:15:19'
    }
    expected = {
        'id': 1,
        'created_by': 'adnankaya', 'context': 'contex1', 'published_date': '2023-05-23 15:15:19+03:00'
    }

    response = client.post('/news/', data=json.dumps(payload))
    assert response.status_code == status.HTTP_201_CREATED
    res_json = response.json()
    assert res_json["created_by"] == expected["created_by"]
    assert res_json["context"] == expected["context"]
    req = dt.strptime(res_json["published_date"], '%Y-%m-%d %H:%M:%S%z')
    res = dt.strptime(expected["published_date"], '%Y-%m-%d %H:%M:%S%z')
    assert req == res

    response = client.post('/news/',
                           json={'created_by': 'cb', 'context': 'ct'})
    assert response.status_code == status.HTTP_422_UNPROCESSABLE_ENTITY
```

위의 테스트 케이스는 news 항목 생성을 위한 `POST /news/` 엔드포인트의 동작을 검증한다. 응답 상태 코드와 응답 데이터를 확인하고 필수 필드가 누락된 경우를 처리한다.

```python
# tests/test_views.py

def test_create_invalid_news():
    response = client.post('/news/', data=json.dumps({'context': 'context2'}))
    assert response.status_code == 422
```

위의 테스트 케이스는 필수 필드가 누락된 잘못된 news 항목이 생성된 경우 서버가 상태 코드가 `422`인 응답을 반환하는지 확인한다.

```python
# tests/test_views.py

def test_read_news_by_id():
    payload = models.NewsSchema(created_by='single_object',
                                context='context single_object',
                                published_date='01-02-2023 13:14:15')
    #  insert database
    obj = insert_t_news(payload)

    # retrieve
    response = client.get(f'/news/{obj["id"]}')
    assert response.status_code == 200
    assert response.json()['id'] == obj['id']
    assert response.json()['created_by'] == obj['created_by']
    assert response.json()['context'] == obj['context']
```

위의 테스트 케이스는 `GET /news/{id}` 엔드포인트가 `id`로 해당 news 항목을 검색하는지, 그리고 응답에 예상 데이터가 포함되어 있는지 확인한다.

```python
# tests/test_views.py

def test_invalid_read_news_by_id():
    response = client.get('/news/44')
    assert response.status_code == 404
    assert response.json()['detail'] == 'News not found'

    response = client.get('/news/0')
    assert response.status_code == 422
```

위의 코드는 `GET /news/{id}` 엔드포인트를 통해 news 항목을 검색할 때 잘못된 `id`가 제공된 경우를 테스트하기 위해 `test_invalid_read_news_by_id`라는 테스트 함수를 정의한다. 이 테스트 케이스는 유효하지 않은 `id`가 제공되었을 때 서버가 존재하지 않는 `id`의 경우 `'News not found'` 상세 메시지와 함께 `404` 상태 코드를 그리고 유효하지 않은 `id` 값의 경우 `422` 상태 코드를 정확히 반환하는지 확인한다.

```python
# tests/test_views.py

def test_update_news():
    payload = {"created_by": "adnankaya_updated",
               "context": "context1_updated",
               "published_date": "01-22-2071 19:18:17"}

    response = client.put("/news/1/", data=json.dumps(payload))
    assert response.status_code == status.HTTP_200_OK
    res_json = response.json()
    assert res_json["created_by"] == payload["created_by"]
    assert res_json["context"] == payload["context"]
    req = dt.strptime(res_json["published_date"], '%Y-%m-%d %H:%M:%S%z')
    res = dt.strptime(payload["published_date"], '%m-%d-%Y %H:%M:%S')
    assert req.year == res.year
    assert req.month == res.month
    assert req.day == res.day
    assert req.hour == res.hour
    assert req.minute == res.minute
    assert req.second == res.second
```

위의 코드는 `id`로 news 항목을 업데이트하기 위해 `PUT /news/{id}` 엔드포인트를 테스트하는 `test_update_news`라는 테스트 함수를 정의한다. 이 테스트 케이스는 `PUT /news/{id}` 엔드포인트가 제공된 데이터로 news 항목을 정확히 업데이트하고 업데이트된 news 항목을 예상 값과 함께 반환하는지 확인한다.

```python
# tests/test_views.py

@pytest.mark.parametrize(
    'id, payload, status_code',
    [
        [1, {}, 422],
        [1, {'context': 'var'}, 422],
        [44, {'created_by': 'adnan', 'context': 'context',
              'published_date': '12-19-2022 13:14:15'}, 404],
        [1, {'created_by': 'c', 'context': 'var'}, 422],
        [1, {'created_by': 'adnan', 'context': 'd'}, 422],
        [0, {'created_by': 'adnan', 'context': 'context'}, 422],
    ],
)
def test_update_invalid_news(id, payload, status_code):
    response = client.put(f'/news/{id}/', data=json.dumps(payload))
    assert response.status_code == status_code
```

위의 코드는 `PUT /news/{id}` 엔드포인트를 통해 유효하지 않은 뉴스 항목을 업데이트하는 다양한 경우를 테스트하기 위해 `test_update_invalid_news`라는 매개변수화된 테스트 함수를 정의한다. 이 매개변수화된 테스트 케이스를 사용하면 필수 필드가 누락되었거나 유효하지 않은 경우를 포함하여 유효하지 않은 news 항목을 업데이트하는 여러 시나리오를 테스트할 수 있다. 각 테스트 케이스는 다양한 시나리오를 다루기 위해 특정 `id`, `payload` 및 예상 `status_code`를 제공한다.

```python
# tests/test_views.py

def test_delete_news_by_id():
    payload = models.NewsSchema(created_by='deleteable_object',
                                context='context deleteable_object',
                                published_date='01-02-2023 13:14:15')
    # insert database
    obj = insert_t_news(payload)
    #  delete request
    response = client.delete(f'/news/{obj["id"]}/')
    assert response.status_code == status.HTTP_204_NO_CONTENT
    assert response.text == ''

    response = client.delete('/news/0/')
    assert response.status_code == status.HTTP_422_UNPROCESSABLE_ENTITY
```

위의 코드는 `id`로 news 항목을 삭제하는 `DELETE /news/{id}` 엔드포인트를 테스트하기 위해 `test_delete_news_by_id`라는 테스트 함수를 정의한다. 이 테스트 케이스는 `DELETE /news/{id}` 엔드포인트가 제공된 `id`로 해당 news 항목을 삭제하고 `204 NO CONTENT` 상태 코드를 반환하는지 확인한다. 또한 유효하지 않은 `id`가 제공되어 `422 UNPROCESSABLE ENTITY` 상태 코드가 예상되는 경우도 처리한다.

```python
# tests/test_views.py

def test_invalid_delete_news_by_id():
    response = client.delete('/news/44/')
    assert response.status_code == status.HTTP_404_NOT_FOUND
    assert response.json()['detail'] == 'News not found'
```

이 코드는` DELETE /news/{id}` 엔드포인트를 통해 news 항목을 삭제하려고 할 때 잘못된 `id`가 제공된 경우를 테스트하기 위해 `test_invalid_delete_news_by_id`라는 테스트 함수를 정의한다. 이 테스트 케이스는 유효하지 않은 `id`가 제공되었을 때 서버가 존재하지 않는 `id`에 대해 `'News not found'` 상세 메시지와 함께 `404` 상태 코드를 반환하는지 확인한다.

### 테스트 실행

```bash
$ pytest
# to hide warning messages you can use
$ pytest -s --disable-warnings
```

아래와 같이 출력된다.

```
============================================================= test session starts =============================================================
platform darwin -- Python 3.11.0, pytest-7.4.0, pluggy-1.2.0
rootdir: /Users/dev/webdev/tutorial/news_fastapi
plugins: anyio-3.7.1
collecting ... Tables are dropped...
Tables are created successfully...
collected 14 items                                                                                                                            

tests/test_views.py ..............

======================================================= 14 passed, 9 warnings in 0.19s ========================================================
```

여기까지이다. 
