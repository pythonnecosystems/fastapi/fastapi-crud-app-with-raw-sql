### 모델과 스키마 생성
`app` 폴더에 `models.py` 파일을 생성하고 다음 코드를 추가한다.

```python
# app/models.py

from pydantic import BaseModel, Field


class NewsSchema(BaseModel):
    created_by: str = Field(..., min_length=3, max_length=140)
    context: str = Field(..., min_length=3, max_length=4096)
    published_date: str = Field(..., min_length=3, max_length=32)


class NewsDB(NewsSchema):
    id: int
```

- `NewsSchema` 클래스를 Pydantic 라이브러리에서 `BaseModel`의 서브클래스로 정의한다. 이 클래스는 news 항목의 스키마 또는 구조를 나타낸다.
- `NewsSchema` 클래스 내부에는 Pydantic의 `Field` 클래스를 사용하여 정의된 세 개의 필드가 있다.
- `created_by` 필드는 문자열(`str`) 타입이며 최소와 최대 길이 제약 조건이 각각 3자와 140자이다.
- `context` 필드 역시 문자열(`str`) 타입이며 최소와 최대 길이 제약 조건은 각각 3자와 4096자이다.
- `published_date` 필드는 문자열(`str`) 타입이며 최소와 최대 길이 제약 조건은 각각 3자와 32자이다.
- `NewsDB` 클래스를 `NewsSchema`의 하위 클래스로 정의한다. 이 클래스는 추가 `id` 필드를 포함하여 news 항목의 데이터베이스 모델을 나타낸다.
- `id` 필드는 정수(`int`) 타입이며 데이터베이스에서 news 항목의 고유 식별자를 나타낸다.

이러한 모델을 정의함으로써 들어오는 데이터의 유효성을 검사하고, JSON을 직렬화/역직렬화하고, 데이터의 구조와 타입에 대한 제약 조건을 적용하는 데 사용할 수 있다.

### `database.py` 파일에 SELECT 쿼리 작성

```python
# app/database.py

# previous codes ...

def select_t_news():
    with PgDatabase() as db:
        db.cursor.execute(f"""SELECT id, created_by, context, published_date 
                         FROM {t_news};""")
        objects = [
            {
                "id": data[0],
                "created_by": data[1],
                "context":data[2],
                "published_date":str(data[3])
            }
            for data in db.cursor.fetchall()
        ]
    return objects
```

- `t_news` 테이블에서 데이터를 검색하도록 `select_t_news` 함수를 정의하였다.
- 이 함수는 `PgDatabase` 클래스의 인스턴스와 함께 `with` 문을 사용하여 컨텍스트 관리자를 사용한다(`with PgDatabase() as db:`). 이렇게 하면 코드 블록을 실행한 후 데이터베이스 연결이 적절하게 관리되고 자동으로 닫힌다.
- `with` 블록 내에서 `db.cursor` 객체의 실행 메서드가 호출되어 `t_news` 테이블에서 데이터를 검색하는 SQL 쿼리를 실행한다. 선택된 열은 `id`, `created_by`, `context` 및 `published_date`이다.
- SQL 쿼리를 실행한 후 `db.cursor`에서 `fetchall` 메서드를 호출하여 선택한 모든 로우(raw)를 튜플 리스트로 검색한다. 각 튜플은 결과 집합의 로우를 나타냅니다.
- list comprehension를 오루을 반복하여 딕셔너리(객체) 리스트를 만드는 데 사용한다. 각 디셔너리는 `"id"`, `"created_by"`, `"context"` 및 `"published_date"`가 키인 news 항목을 나타낸다. 이 값은 튜플의 해당 요소에서 추출된다. `str()`을 사용하여 `"published_date"` 값을 문자열로 변환한다.
- 마지막으로 선택된 news 항목을 딕셔너리으로 표현한 `objects` 리스트를 반환한다.

### `database.py` 파일에 ID를 사용한 SELECT 쿼리 작성

```python
# app/database.py

# previous codes ...

def select_t_news_by_id(id: int) -> dict:
    with PgDatabase() as db:
        db.cursor.execute(f"""
        SELECT id, created_by, context, published_date FROM {t_news}
        WHERE id={id};
                        """)
        data = db.cursor.fetchone()
        if data is None:
            return None

    return {
        "id": data[0],
        "created_by": data[1],
        "context": data[2],
        "published_date": str(data[3])
    }
```

- 제공된 아이디를 기준으로 `t_news` 테이블에서 특정 news 항목을 검색하도록 `select_t_news_by_id` 함수를 정의하고 있다.
- 쿼리에는 제공된 아이디와 일치하는 아이디 컬럼을 기준으로 로우를 필터링하는 `WHERE` 절을 포함하고 있다.
- SQL 쿼리를 실행한 다음. `db.cursor`에서 `fetchone` 메서드가 호출되어 쿼리와 일치하는 첫 번째 로우를 검색한다. 결과는 선택한 로우를 나타내는 튜플을 데이터 변수에 저장된다.
- `data`가 `None`이면 제공된 ID를 가진 로우를 찾지 못했음을 의미하므로 `None`이 반환된다.
- `data`에 유효한 로우가 포함되어 있으면 `"id"`, `"created_by"`, `"context"` 및 `"published_date"` 키를 사용하여 딕셔너리가 만들어진다. 값은 튜플의 해당 요소에서 추출된다.` "published_date"` 값은 `str()`을 사용하여 문자열로 변환된다.
- 마지막으로 선택한 news 항목을 나타내는 딕셔너리를 반환한다.

### `database.py` 파일에 INSERT 쿼리 작성

```python
# app/database.py

# previous imports ...
# internals
from app.models import NewsDB, NewsSchema # new

# previous codes ...

def insert_t_news(payload: NewsSchema, *args, **kwargs) -> NewsDB:
    with PgDatabase() as db:
        db.cursor.execute(f"""
        INSERT INTO {t_news}(created_by, context, published_date) 
        VALUES('{payload.created_by}', 
                '{payload.context}', 
                '{payload.published_date}'
                ) 
        RETURNING id;
                    """)
        db.connection.commit()
        inserted_id = db.cursor.fetchone()[0]

        obj = select_t_news_by_id(inserted_id)
    return obj
```

- `with` 블록 내에서 `db.cursor` 객체의 `execute` 메서드가 호출되어 `t_news` 테이블에 새 로우를 삽입하는 SQL 쿼리를 실행한다. 쿼리는 `payload` 객체에서 추출한 값이 포함된 `VALUES` 절을 포함한다.
- `RETURNING` 절은 새로 삽입된 로우의 ID를 검색하는 데 사용된다.
- SQL 쿼리를 실행한 후 `db.connection.commit()`을 사용하여 변경 사항을 데이터베이스에 커밋하여 삽입을 영구적으로 유지한다.
- `fetchone` 메서드는 `db.cursor`에서 호출되어 삽입된 로우의 `id` 값을 검색한다. 이 값을 `inserted_id` 변수에 저장한다.
- `inserted_id`와 함께 `select_t_news_by_id` 함수가 호출되어 삽입된 news 항목을 검색한다.
- 마지막으로 삽입된 news 항목을 반환한다.

### `database.py` 파일에 UPDATE 쿼리 작성

```python
# app/database.py

# previous codes
def update_t_news_by_id(id: int, payload: NewsSchema):
    with PgDatabase() as db:
        db.cursor.execute(f"""
        UPDATE {t_news}
        SET created_by='{payload.created_by}', 
            context='{payload.context}', 
            published_date='{payload.published_date}'
        WHERE id='{id}'
        RETURNING id;
                        """)
        db.connection.commit()
        result = db.cursor.fetchone()
        if not result:
            return None
        updated_id = result[0]
        obj = select_t_news_by_id(updated_id)
    return obj
```

### `database.py` 파일에 DELETE 쿼리 작성

```python
# app/database.py

# previous codes...
def delete_t_news_by_id(id: int):
    with PgDatabase() as db:
        db.cursor.execute(f"""
        DELETE FROM {t_news}
        WHERE id={id};
                        """)
        db.connection.commit()
        res = db.cursor.statusmessage
    if res == "DELETE 1":
        return True
    return False
```

- `db.cursor` 객체의 `statusmessage` 속성을 확인하여 삭제 작업의 결과를 확인한다. `statusmessage`가 `"DELETE 1"`이면 한 로우가 성공적으로 삭제되었음을 의미하므로 `True`를 반환한다.
- `statusmessage`가 `"DELETE 1"`이 아니라면 삭제된 로우가 없거나 오류가 발생했음을 의미하므로 `False`를 반환한다.

### CRUD view 작성
`app` 폴더에 `veiws.py` 파일을 만들고, **`create_news`**함수를 작성한다.

```python
# app/views.py

from fastapi import APIRouter, HTTPException, status
from psycopg2.errors import DatetimeFieldOverflow
#  internals
from app.database import (insert_t_news,)
from app.models import NewsDB, NewsSchema

router = APIRouter()


@router.post('/', response_model=NewsDB, status_code=status.HTTP_201_CREATED)
async def create_news(payload: NewsSchema):
    try:
        res = insert_t_news(payload)
        return res
    except DatetimeFieldOverflow:
        raise HTTPException(
            status_code=status.HTTP_400_BAD_REQUEST,
            detail="Formats are : month-day-year hour:minute:seconds or year-month-day hour:minute:seconds"
        )
```

- 예외를 처리하기 위해 FastAPI에서 `HTTPException` 클래스를 import 한다.
- `psycopg2.errors`에서 `DatetimeFieldOverflow` 예외 클래스를 import한다. 이 클래스는 특히 datetime 필드 오버플로우와 관련된 예외를 처리한다.
- `app.database` 모듈에서 `insert_t_news` 함수를 import한다. 이 함수는 데이터베이스에 news 항목을 삽입하는 데 사용된다.
- `app.models` 모듈에서 `NewsDB`와 `NewsSchema` 모델을 import한다. 각각 response 모델과 payload 모델로 사용된다.
- `APIRouter` 인스턴스가 생성되어 `router` 변수에 할당된다.
- `@router.post` 데코레이터는 news 항목을 만들기 위한 POST 경로를 정의하는 데 사용된다. 이 데코레이터는 루트 경로("/")를 엔드포인트로 지정한다.
- `response_model` 매개 변수는 이 경로에 대한 응답 모델을 나타내는 `NewsDB`로 설정된다. 이 매개변수는 응답이 `NewsDB` 모델에 따라 직렬화되도록 한다.
- `status_code` 매개 변수는 응답 상태 코드가 `201 created`을 나타내는 `status.HTTP_201_CREATED`로 설정된다.
- `create_news`라는 이름으로 route 함수를 정의한다. 이 함수는 요청 바디에서 전송된 데이터를 나타내는 `NewsSchema` 타입의 `payload` 매개변수를 받는다.
- 함수 내부에서는 `insert_t_news(payload)`를 호출하여 news 항목을 데이터베이스에 삽입하려고 시도한다. 결과를 `res` 변수에 저장한다.
- 삽입이 성공하면 `res` 변수가 응답으로 반환된다.
- 삽입하는 동안 `DatetimeFieldOverflow` 예외가 발생하면 제공된 datetime 값이 필드의 용량을 초과한다는 의미이다. 이 경우 상태 코드 `400 Bad Request`과 지원되는 datetime 형식을 나타내는 세부 메시지와 함께 `HTTPException`이 발생한다.

이 코드는 적절한 응답 모델과 datetime 필드 오버플로우에 대한 오류 처리를 통해 news 항목 생성을 처리하는 API 경로를 설정한다.

**`app/main.py`에 다음과 같이 `/news` 엔드포인트를 포함해야 한다.**

```python
# app/main.py

from fastapi import FastAPI, status
from fastapi.exceptions import HTTPException
# internals
from app.database import drop_tables, create_tables
from app import views # new

app = FastAPI()

# previous codes...

# at the end of the file
app.include_router(views.router, prefix='/news', tags=['news'])
```

`create_news`를 테스트하려면 바디와 함께 http://0.0.0.0:8000/news/ 주소로 POST 요청을 보내야 한다.

```json
{
  "created_by":"adnankaya",
  "context":"fastapi tutorial by adnankaya",
  "published_date":"2023-07-14 14:53:21"
}
```

응답은 아래와 같다.

```json
{
  "created_by": "adnankaya",
  "context": "fastapi tutorial by adnankaya",
  "published_date": "2023-07-14 14:53:21+03:00",
  "id": 1
}
```

**read_news**

```python
# app/views.py

from typing import List # new
from fastapi import APIRouter, HTTPException, status
from psycopg2.errors import DatetimeFieldOverflow, OperationalError # new
#  internals
from app.database import (insert_t_news, select_t_news) # new
from app.models import NewsDB, NewsSchema

# previous codes...

@router.get('/', response_model=List[NewsDB], status_code=status.HTTP_200_OK)
async def read_news():
    try:
        return select_t_news()
    except OperationalError:
        raise HTTPException(
            status_code=status.HTTP_500_INTERNAL_SERVER_ERROR,
            detail="""Check if the database exists, connection is successful or tables exist. To create tables use '/initdb' endpoint"""
        )
    except Exception as e:
        raise HTTPException(
            status_code=status.HTTP_400_BAD_REQUEST,
            detail=f"""Error {e}"""
        )
```

- `@router.get` 데코레이터는 news 항목을 검색하기 위한 GET 경로를 정의하는 데 사용된다. 루트 경로("/")가 엔드포인트로 사용된다.
- `response_model` 매개변수는 `List[NewsDB]`로 설정되어 응답이 `NewsDB` 모델에 따라 직렬화된 news 항목의 리스트가 될 것임을 나타낸다.
- `status_code` 매개변수는 `status.HTTP_200_OK`로 설정되어 응답 상태 코드가 `200 OK`가 될 것임을 나타낸다.
- 경로 함수를 `read_news`라는 이름과 매개변수 없이 정의한다.
- 함수 내에서 `select_t_news` 함수를 호출하여 모든 news 항목을 검색하려고 한다.
- 검색에 성공하면 검색된 news 항목을 응답으로 반환한다.
- 검색 중에 `OperationalError` 예외가 발생하면 데이터베이스와 관련된 서버 측 문제가 발생한 것이다. 이 경우 상태 코드 `500 Internal Server Error` 및 데이터베이스의 존재 여부, 연결 성공 여부 또는 테이블 존재 여부를 확인하라는 세부 메시지와 함께 `HTTPException`이 발생한다. 또한 테이블 생성을 위한 `/initdb` 엔드포인트도 언급된다.
- 검색 중에 다른 예외가 발생하면 상태 코드가 `400 Bad Request`인 일반 `HTTPException`이 발생한다. 예외 세부 정보는 응답의 상세 메시지에서 확인할 수 있다.

이 코드는 적절한 응답 모델과 작동 오류 및 기타 예외에 대한 오류 처리를 통해 모든 news 항목을 검색하는 GET 경로를 설정한다.

`read_news`를 테스트하려면 http://0.0.0.0:8000/news/ 주소로 GET 요청을 보낸다.

다음과 같은 응답을 받게 될 것이다.

```json
[
  {
    "created_by": "adnankaya",
    "context": "fastapi tutorial by adnankaya",
    "published_date": "2023-07-14 14:53:21+03:00",
    "id": 1
  },
  {
    "created_by": "adnankaya",
    "context": "django tutorial by adnankaya",
    "published_date": "2023-06-14 14:53:21+03:00",
    "id": 2
  },
  {
    "created_by": "adnankaya",
    "context": "flask tutorial by adnankaya",
    "published_date": "2022-11-22 14:53:21+03:00",
    "id": 3
  }
]
```

**read_news_by_id**

```python
# app/views.py

from typing import List
from fastapi import APIRouter, HTTPException, status, Path # new
from psycopg2.errors import DatetimeFieldOverflow, OperationalError
#  internals
from app.database import (insert_t_news, select_t_news, select_t_news_by_id) # new
from app.models import NewsDB, NewsSchema

@router.get('/{id}/', response_model=NewsDB, status_code=status.HTTP_200_OK)
async def read_news_by_id(id: int = Path(..., gt=0)):
    result = select_t_news_by_id(id)
    if not result:
        raise HTTPException(
            status_code=status.HTTP_404_NOT_FOUND, detail='News not found')
    return result
```

- `@router.get` 데코레이터는 `id`로 news 항목을 검색하기 위한 GET 경로를 정의하는 데 사용된다. 경로의 URL에 `/{id}/` 경로 매개변수를 지정한다.
- `response_model` 매개변수는 `NewsDB`로 설정되어 응답이 `NewsDB` 모델에 따라 직렬화된 news 항목임을 나타낸다.
- `status_code` 매개변수는 `status.HTTP_200_OK`로 설정되어 응답 상태 코드가 `200 OK`임을 나타낸다.
- `read_news_by_id`라는 이름으로 경로 함수를 정의하며, `int` 타입의 `id` 매개변수를 받는다. `Path` 클래스는 URL의 경로 파라미터로 파라미터를 정의하기 위해 `fastapi.Path`로부터 사용된다.
- `id` 매개변수는 `Path` 생성자에서 `...`와 `gt=0`을 사용하여 유효성을 검사하며, 이는 `id`가 제공되어야 하고 `0`보다 커야 함을 나타낸다.
- 함수 내에서 `select_t_news_by_id` 함수가 호출되어 제공된 `id`를 기반으로 news 항목을 검색한다. 결과는 `result` 변수에 저장된다.
- 제공된 `id`가 news 항목에 없어 `result`가 `None`인 경우 상태 코드 `404 Not Found`와 함께 news 항목을 찾을 수 없음을 나타내는 상세 메시지가 포함된 `HTTPException`이 발생한다.
- 결과가 `None`이 아닌 경우 news 항목이 응답으로 반환된다.

news 항목을 찾을 수 없는 경우 적절한 응답 모델 및 오류 처리와 함께 이 코드는 특정 news 항목을 `id`로 검색하는 GET 경로를 설정한다.

`read_news_by_id`를 테스트하려면 http://0.0.0.0:8000/news/1 주소로 GET 요청을 보낸다.

다음과 같은 응답을 받게 될 것이다.

```json
{
  "created_by": "adnankaya",
  "context": "fastapi tutorial by adnankaya",
  "published_date": "2023-07-14 14:53:21+03:00",
  "id": 1
}
```

**update_news_by_id**

```python
# app/views.py

#  internals
from app.database import (insert_t_news, select_t_news, select_t_news_by_id, 
                          update_t_news_by_id) # new


@router.put('/{id}/', response_model=NewsDB, status_code=status.HTTP_200_OK)
async def update_news_by_id(payload: NewsSchema, id: int = Path(..., gt=0)):
    result = update_t_news_by_id(id, payload)
    if not result:
        raise HTTPException(
            status_code=status.HTTP_404_NOT_FOUND, detail='News not found')
    return result
```

- `@router.put` 데코레이터는 `id`로 news 항목을 업데이트하기 위한 PUT 경로를 정의하는 데 사용된다. 경로의 URL에 `/{id}/` 경로 매개변수를 지정한다.
- `response_model` 매개변수는 `NewsDB`로 설정되어 응답이 `NewsDB` 모델에 따라 직렬화된 news 항목임을 나타낸다.
- `status_code` 매개변수는 `status.HTTP_200_OK`로 설정되어 응답 상태 코드가 `200 OK`임을 나타낸다.
- `update_news_by_id`라는 이름으로 경로 함수를 정의하며, 요청 바디에서 전송된 데이터를 나타내는 `NewsSchema` 타입의 `payload` 매개변수와 news 항목을 나타내는 `int` 타입의 `id` 매개변수를 받는다. 두 파라매터 모두 `id`가 제공되어야 하고 `0`보다 큼을 보장하기 위하여 `...`와 `gt=0`을 사용하여 유효성을 검사한다.
- 함수 내에서 `update_t_news_by_id` 함수가 호출되어 제공된 `id`를 기반으로 news 항목을 갱신한다. 결과는 `result` 변수에 저장된다.
- 제공된 `id`가 news 항목에 없어 `result`가 `None`인 경우 상태 코드 `404 Not Found`와 함께 news 항목을 찾을 수 없음을 나타내는 상세 메시지가 포함된 `HTTPException`이 발생한다.
- 결과가 `None`이 아닌 경우 news 항목이 응답으로 반환된다.

news 항목을 찾을 수 없는 경우 적절한 응답 모델 및 오류 처리와 함께 이 코드는 특정 news 항목을 `id`로 업데이트하는 PUT 경로를 설정한다.

view를 테스트하려면 바디과 함께 http://0.0.0.0:8000/news/1 으로 PUT 요청을 보낸다.

```json
{
  "created_by": "adnankaya new",
  "context": "fastapi & raw SQL tutorial by adnankaya",
  "published_date": "2023-07-11 14:53:21+03:00"
}
```

다음과 같은 응답을 받게 될 것이다.

```json
{
  "created_by": "adnankaya new",
  "context": "fastapi & raw SQL tutorial by adnankaya",
  "published_date": "2023-07-11 14:53:21+03:00",
  "id": 1
}
```

**delete_news_by_id**

```python
# app/views.py

#  internals
from app.database import (insert_t_news, select_t_news, select_t_news_by_id, 
                          update_t_news_by_id, delete_t_news_by_id) # new


@router.delete('/{id}/', status_code=status.HTTP_204_NO_CONTENT)
async def delete_news_by_id(id: int = Path(..., gt=0)):
    result = delete_t_news_by_id(id)
    if not result:
        raise HTTPException(
            status_code=status.HTTP_404_NOT_FOUND, detail='News not found')
    return result
```

- `@router.delete` 데코레이터는 `id`로 news 항목을 삭제하기 위한 DELETE 경로를 정의하는 데 사용된다. 경로의 URL에 `/{id}/` 경로 매개변수를 지정한다.
- `status_code` 매개변수는 `status.HTTP_204_NO_CONTENT`로 설정되어 응답 상태 코드가 `204 No Content`임을 나타낸다.
- `delete_news_by_id`라는 이름으로 경로 함수를 정의하며, `int` 타입의 `id` 매개변수를 받는다. `Path` 클래스는 URL의 경로 파라미터로 파라미터를 정의하기 위해 사용되며, `id`가 제공되어야 하고 `0`보다 큼을 보장하기 위하여 `...`와 `gt=0`을 사용하여 유효성을 검사한다.
- 함수 내에서 `delete_t_news_by_id` 함수가 호출되어 제공된 `id`를 기반으로 news 항목을 갱신한다. 결과는 `result` 변수에 저장된다.
- 제공된 `id`가 news 항목에 없거나 삭제가 실패하여 `result`가 `False`인 경우 상태 코드 `404 Not Found`와 함께 news 항목을 찾을 수 없음을 나타내는 상세 메시지가 포함된 `HTTPException`이 발생한다.
- 삭제를 성공하여 결과가 `True`인 경우 `result`를 결과로 반환한다. 그러나 응단 상태 코드가 `204 No Content`이면, 응답 바디는 비어 있다.

news 항목을 찾을 수 없는 경우 적절한 응답 모델 및 오류 처리와 함께 이 코드는 특정 news 항목을 `id`로 삭제하는 DELETE 경로를 설정한다.

http://0.0.0.0:8000/news/2 주소로 DELETE 요청을 보내 view를 테스트한다. 응답은 비어 있고 **`Status: 204 No Content`**이다.
