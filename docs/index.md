# Raw SQL이 포함된 FastAPI CRUD 앱 <sup>[1](#footnote_1)</sup>

이 포스팅에서는 psycopg2 라이브러리와 함께 raw SQL 쿼리를 사용하여 FastAPI CRUD(Create, Read, Update, Delete) 어플리케이션을 구축하는 방법을 살펴본다. 또한 FastAPI 어플리케이션에서 raw SQL을 사용할 때와 ORM 라이브러리를 사용할 때의 장단점을 비교해 보겠다. 이 글을 읽고 FastAPI에서 raw SQL을 활용하는 방법을 확실히 이해하고 프로젝트에 적합한 접근 방식인지 판단할 수 있기를 바란다.

목차:

1. [FastAPI와 psycopg2 소개](./intro.md)
1. [raw SQL의 장단점](./pros-and-ons-of-raw-sql.md)
1. [프로젝트 설정하기](./setting-up-project.md)
1. [PostgreSQL 데이터베이스에 연결하기](./connecting-to-postgresql.md)
1. [raw SQL로 CRUD 엔드포인트 구축하기](./building-crud-endpoints.md)
1. [FastAPI CRUD 앱 테스트](./testing-fastapi-crud-app.md)
1. [맺는 말]()


<a name="footnote_1">1</a>: 이 페이지는 [FastAPI CRUD app with Raw SQL | Part 1](https://medium.com/@adnan-kaya/fastapi-crud-app-with-raw-sql-part-1-d62bad80a386), [FastAPI CRUD app with Raw SQL | Part 2](https://medium.com/@adnan-kaya/fastapi-crud-app-with-raw-sql-part-2-93b0e1cf73f5) 와 [FastAPI CRUD app with Raw SQL | Part 3](https://medium.com/@adnan-kaya/fastapi-crud-app-with-raw-sql-part-3-a944e2851b5a)을 편역한 것임.
