**`news_fastapi`** 폴더를 생성하고 즐겨 사용하는 코드 편집기를 연다. 터미널을 열고 `news_fastapi` 디렉토리로 이동한다. Python 프로젝트를 위한 가상 환경을 만드는 것을 권한다.

```bash
$ cd news_fastapi
# create virtual environment using venv module(-m) and name it as venv
$ python3.11 -m venv venv
# activate venv
$ source venv/bin/activate
# windows users can activate like the following
$ venv\Scripts\activate
```

## FastAPI 설치

```bash
# after activating venv use pip to install fastapi
(venv) $ pip install fastapi
```

또한 서버로 작동할 `uvicorn`을 설치한다.

```bash
(venv) $ pip install "uvicorn[standard]"
```

`news_fastapi` 폴더 아래에 `app`과 `test` 폴더를 만든다. 현재 프로젝트 파일과 폴더 구조는 다음과 같다.

```
news_fastapi
├──  requirements.txt
├── app
└── tests
└── venv
```

`app` 폴더에 `main.py`를 생성하고 다음 코드를 추가한다.

```python
# app/main.py

from fastapi import FastAPI

app = FastAPI()


@app.get('/')
async def index():
    return {'message': 'Welcome to news app!'}
```

이 코드는 루트 경로("/")에 대해 단일 경로가 정의된 기본 FastAPI 어플리케이션을 설정한 것이다. 각 부분이 수행하는 작업은 다음과 같다.

- `from fastapi import FastAPI`: 이 줄은 `fastapi` 모듈에서 `FastAPI` 클래스를 임포트한다. `FastAPI`는 어플리케이션을 만드는 데 사용할 기본 클래스이다.
- `app = FastAPI()`: 이 줄은 `FastAPI` 클래스의 인스턴스를 생성하고 이를 앱 변수에 할당한다. 이 앱 객체를 사용하여 경로를 정의하고 어플리케이션을 실행할 것이다.
- `@app.get('/')`: 이는 루트 경로("/")로 GET 요청이 이루어질 때 다음 함수를 실행해야 한다고 FastAPI에 알려주는 데코레이터이다.
- `async def index():`: 이는 함수 `index`를 비동기 함수로 정의하는 것이다. `async` 키워드를 사용하면 함수 내에서 `await`을 사용할 수 있다.
- `return {'message': 'Welcome to news app!'}`: 이 줄은 키가 "`message`"이고 값이 "`Welcome to news app!`"인 단일 키-값 쌍이 포함된 딕셔너리를 반환한다. FastAPI는 자동으로 사전을 JSON 형식으로 직렬화하여 응답으로 전송한다.

요약하자면, 이 코드는 GET 요청을 통해 액세스할 때 JSON 메시지로 응답하는 단일 경로(`/`)를 가진 FastAPI 어플리케이션을 설정한 것이다. 그 메시지는 Welcome to news app!"라는 간단한 인사말이다.

## app 실행

```bash
$ uvicorn app.main:app --reload --workers 1 --host 0.0.0.0 --port 8000
```

`uvicorn app.main:app` 명령은 다음을 의미한다.

- `main`: `app` 폴더에 있는 `main.py` 파일(Python "모듈").
- `app`:` app = FastAPI()` 줄을 사용하여 `main.py` 내부에 생성된 객체.
- `--reload`: 코드 변경 후 서버를 다시 시작하게 한다. 개발용으로만 사용하여야 한다.
- `--workers 1`: 작업자 수
- `--host`: 호스트 IP 주소
- `--port`: 포트 번호

브라우저를 열고 http://0.0.0.0:8000/ 주소로 이동한다. json 응답 메시지 `{"message":"Welcome to news app!"}`를 출력한다.
