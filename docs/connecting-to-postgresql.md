.env 파일에서 환경 변수를 읽기 위해 이미 **uvicorn**과 함께 설치되어 있는 **python-dotenv** 패키지를 사용한다.

`app` 폴더 아래에 `database.py` 파일을 생성한다.

```python
# app/database.py

import os
from pathlib import Path
import dotenv

BASE_DIR = Path(__file__).resolve().parent.parent
dotenv.load_dotenv(BASE_DIR / ".env")
```

- `pathlib`의 `Path` 클래스를 사용하여 `BASE_DIR`을 정의한다. 현재 파일(`__file__`)의 경로를 확인하고 두 단계 위로(`parent.parent`) 이동하면 프로젝트의 기반(base) 디렉터리이디.
- `.env` 파일의 경로(`BASE_DIR / ".env"`)를 인수로 사용하여 `dotenv.load_dotenv()` 함수를 호출한다. 이 함수는 `.env` 파일에서 환경 변수를 로드하여 어플리케이션에서 사용할 수 있도록 한다.

프로젝트 디렉터리에 `.env` 파일이 있고 이 파일에 필요한 환경 변수가 포함되어 있는지 확인하세요. `dotenv.load_dotenv()` 함수는 이 파일에서 환경 변수를 어플리케이션의 환경으로 로드한다.

데이터베이스 작업에 사용할 컨텍스트 관리자 기본 클래스를 만들어 보자. 

**app/database.py**

```python
# app/database.py

import os
from pathlib import Path
import dotenv
from abc import ABC, abstractmethod # new

BASE_DIR = Path(__file__).resolve().parent.parent
dotenv.load_dotenv(BASE_DIR / ".env")

class Database(ABC):
    """
    Database context manager
    """

    def __init__(self, driver) -> None:
        self.driver = driver

    @abstractmethod
    def connect_to_database(self):
        raise NotImplementedError()

    def __enter__(self):
        self.connection = self.connect_to_database()
        self.cursor = self.connection.cursor()
        return self

    def __exit__(self, exception_type, exc_val, traceback):
        self.cursor.close()
        self.connection.close()
```

- `Database` 클래스는 Abstract Base Class의 약자인 ABC 클래스의 서브클래스로 정의된다. 이 클래스는 다른 클래스의 베이스 클래스 역할을 하며 하위 클래스에서 구현해야 하는 추상 메서드를 정의할 수 있다.
- `__init___` 메서드는 `Database` 클래스의 생성자이다. 이 메서드는 데이터베이스 드라이버를 나타내는 `driver` 매개변수를 받는다. `self.driver` 속성은 제공된 드라이버 값으로 설정된다.
- `@abstractmethod` 데코레이터를 사용하여 `connect_to_database` 메서드를 추상 메서드로 표시한다. `Database`의 서브클래스는 이 메서드를 구현해야 하며, 이 메서드는 특정 데이터베이스에 대한 연결을 설정해야 한다.
- `__enter__` 메서드는 Python의 컨텍스트 관리자 프로토콜에서 사용되는 특수 메서드이다. 이 메서드는 컨텍스트에 들어갈 때(`with` 문을 사용하여) 호출된다. 여기서는 `connect_to_database` 메서드를 호출하여 데이터베이스 연결을 설정하고 `self.connection`에 연결을 할당한다. 또한 커서 객체(`self.cursor`)를 생성하여 SQL 쿼리를 실행한다.
- `__enter__` 메서드는 `self`를 반환하며, 이를 통해 `Database` 클래스의 인스턴스를 `with` 블록 내에서 컨텍스트 관리자로 사용할 수 있다.
- `__exit__` 메서드는 컨텍스트 관리자 프로토콜에서 사용되는 또 다른 특수 메서드이다. 이 메서드는 컨텍스트를 종료할 때(`with` 블록을 떠날 때) 호출된다. 이 메서드는 커서(`self.cursor`)와 데이터베이스 연결(`self.connection`)을 닫는다.
- `__exit__` 메서드는 `exception_type`, `exc_val` 및 `traceback` 세 가지 인수를 받는다. 이 인수는 컨텍스트 내에서 발생한 모든 예외를 처리하는 데 사용된다. 그러나 이 코드에서는 명시적인 예외 처리가 구현되어 있지는 않다.

요약하자면, 이 코드는 데이터베이스 작업을 위한 컨텍스트 관리자 역할을 하는 `Database` 클래스를 정의한다. 이 클래스는 하위 클래스에서 `connect_to_database` 메서드를 구현하여야 하며, 데이터베이스 연결과 커서에 필요한 설정과 정리 로직을 제공한다.

### PostgreSQL 작동을 위한 psycopg2 패키지 설치

```bash
$ pip install psycopg2
```

> **Note**:<br>
> postgreSQL을 설치하고 *t_news*라는 데이터베이스를 만들었다고 가정한다.

이제 `app/database.py`를 계속 편집해 보겠다.

```python
# app/database.py

# previous imports
import psycopg2

# previous codes ...

class PgDatabase(Database):
    """PostgreSQL Database context manager"""

    def __init__(self) -> None:
        self.driver = psycopg2
        super().__init__(self.driver)

    def connect_to_database(self):
        return self.driver.connect(
            host=os.getenv("DB_HOST"),
            port=os.getenv("DB_PORT"),
            user=os.getenv("DB_USERNAME"),
            password=os.getenv("DB_PASSWORD"),
            database=os.getenv("DB_NAME")
        )
```

- `PgDatabase` 클래스를 `Database` 클래스의 서브클래스로 정의한다. 이 클래스는 특히 PostgreSQL 데이터베이스 컨텍스트 관리자를 나타낸다.
- `__init__` 메서드는 `PgDatabase` 객체를 초기화한다. 이 메서드는 `self.driver` 속성을 PostgreSQL 드라이버 라이브러리인 `psycopg2`로 설정한다. 그런 다음 `super().__init__(self.driver)`를 사용하여 부모 `Database` 클래스의 `__init__` 메서드를 호출하고, `psycopg2` 드라이버를 인수로 전달한다.
- 여기서 구현된 `connect_to_database` 메서드는 `psycopg2` 드라이버를 사용하여 PostgreSQL 데이터베이스에 대한 연결을 설정한다. 이 메서드는 `DB_HOST`, `DB_PORT`, `DB_USERNAME`, `DB_PASSWORD` 및 `DB_NAME` 같은 환경 변수(`os.getenv`)에서 연결 세부 정보를 검색한다. 이러한 환경 변수을 데이터베이스 구성으로 적절히 설정해야 한다.
- 검색된 연결 세부 정보로 `psycopg2`의 `connect` 메서드를 호출하여 연결을 설정한다. 이 메서드는 연결 객체를 반환한다.
- `.env` 파일을 생성하고 다음을 추가한다(자격 증명에 따라 값을 변경하여야 한다).

```bash
export DB_HOST=localhost
export DB_PORT=5432
export DB_USERNAME=developer
export DB_PASSWORD=developer
export DB_NAME=db_news
```

요약하자면, 이 코드는 PostgreSQL 데이터베이스 관리를 위해 특별히 맞춤화된 `Database`의 서브클래스인 `PgDatabase` 클래스를 정의한다. 이 코드는 PostgreSQL 드라이버(`psycopg2`)를 설정하고 `connect_to_database` 메서드를 구현하여 드라이버에 연결을 위한 세부 정보인 환경 변수를 사용하여 데이터베이스에 대한 연결을 설정한다.

### 테이블 생성
`app/database.py`에 다음 함수와 테이블 이름 변수를 추가한다.

```python
# app/database.py

# previous codes

t_news = "t_news"

def create_tables():
    with PgDatabase() as db:
        db.cursor.execute(f"""CREATE TABLE {t_news} (
            id SERIAL PRIMARY KEY,
            published_date TIMESTAMPTZ,
            created_date TIMESTAMPTZ DEFAULT NOW(),
            created_by VARCHAR(140),
            context TEXT NOT NULL
            );
        """)
        db.connection.commit()
        print("Tables are created successfully...")
```

- 데이터베이스에 필요한 테이블을 생성하기 위해 `create_tables` 함수를 정의한다.
- 이 함수는 `PgDatabase` 클래스의 인스턴스와 함께 `with` 문을 사용하여 컨텍스트 관리자를 사용한다(`with PgDatabase() as db:`). 이렇게 하면 코드 블록을 실행한 후 데이터베이스 연결이 적절하게 관리되고 자동으로 닫힌다.
- `with` 블록 내에서 `db.cursor` 객체의 실행 메서드가 호출되어 테이블을 생성하는 SQL 쿼리를 실행한다. 테이블 이름은 `t_news` 변수에 저장될 것으로 예상된다(코드에 정의되어 있다고 가정한다). `CREATE TABLE` 문은 `id`, `published_date`, `created_date`, `created_by` 및 `context` 컬럼으로 테이블 구조를 정의한다.
- `CREATE TABLE` 문을 실행하면 `db.connection.commit()`을 실행하여 변경 사항을 데이터베이스에 커밋한다. 이렇게 하면 테이블 생성 작업이 영구적으로 유지된다.
- 마지막으로 테이블이 성공적으로 생성되었음을 나타내는 성공 메시지가 콘솔에 출력된다.

### 데이블 삭제
테이블이 이미 있는 경우 테이블을 삭제하는 다른 함수를 `database.py`에 추가한다.

```python
# app/database.py

# previous codes

def drop_tables():
    with PgDatabase() as db:
        db.cursor.execute(f"DROP TABLE IF EXISTS {t_news} CASCADE;")
        db.connection.commit()
        print("Tables are dropped...")
```

### 엔트포인트를 통한 테이블 삭제와 생성
테이블을 삭제하고 생성할 엔드포인트를 만들어 보자. `app/main.py`를 열고 다음 코드를 추가한다.

```python
# app/main.py

from fastapi import FastAPI, status # new
from fastapi.exceptions import HTTPException # new
# internals
from app.database import drop_tables, create_tables # new

app = FastAPI()

# previous codes ...

@app.post('/initdb')
async def initdb():
    try:
        drop_tables()
        create_tables()
        return {"message": "Tables dropped and created!"}
    except Exception as e:
        raise HTTPException(
            status_code=status.HTTP_400_BAD_REQUEST,
            detail=f"Error {e}"
        )
```

- 예외를 처리하는 데 필요한 모듈(`fastapi.exceptions.HTTPException`)과 `app.database` 모듈에서 `drop_tables`와 `create_tables` 함수를 호출할 수 있도록 `import` 문을 추가한다.
- `@app.post` 데코레이터를 사용하여 HTTP POST 경로로 `/initdb` 경로를 정의한다. 이는 데이터베이스 테이블을 삭제하고 생성하는 역할을 수행한다.
- 경로 함수 내부에서는 테이블을 삭제하고 생성하는 동안 발생할 수 있는 모든 잠재적 예외를 처리하기 위해 `try-except` 블록을 사용한다.
- `try` 블록에서는 `drop_tables` 함수를 호출하여 기존 테이블을 삭제한 다음 `create_tables` 함수를 호출하여 새 테이블을 생성한다.
- 예외 없이 작업이 완료되면 성공 메시지 `{"message": "Tables dropped and created!"}`와 함께 JSON 응답을 반환한다. .
- 실행 중 예외가 발생하면 `except` 블록이 트리거된다. 상태 코드 `400`(Bad Request)과 특정 오류를 포함하는 자세한 오류 메시지(`f"error {e}"`)와 함께 `HTTPException`이 발생된다.

이제 엔드포인트를 테스트해 보겠다. **curl** 또는 [postman](https://meetup.nhncloud.com/posts/107)을 사용하기 위하여 터미널을 열거나, [thunder client](https://webruden.tistory.com/840)를 사용하여 엔드포인트로 POST 요청을 보낸다.

```bash
$ curl -X POST http://0.0.0.0:8000/initdb
```

아래와 같은 응답 메시지를 얻을 수 있다.

```
{"message":"Tables dropped and created!"}
```


